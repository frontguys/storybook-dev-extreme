import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxButtonModule } from 'devextreme-angular';

/**
 * An <oddo-button> represents an action a user can trigger, they can be clicked or tapped to perform any action.
 */
@Component({
  selector: 'oddo-button',
  standalone: true,
  imports: [CommonModule, DxButtonModule],
  template: `
    <dx-button
      text="{{ label }}"
      [icon]="_variants.defaultIcon[variant] || icon !== 'none' && icon"
      [type]="_variants.type[variant] || 'default'"
      [stylingMode]="_variants.stylingMode[variant] || 'contained'"
      (click)="_onClick"
      [disabled]="disabled"
      cornerRadius="4"
    ></dx-button>
  `,
})
export class ButtonComponent {
  /**
   * Button's text content
   */
  @Input() label: string = '';

  /**
   * Icon  displayed before button's text
   * https://js.devexpress.com/Angular/Documentation/Guide/Themes_and_Styles/Icons
   */
  @Input() icon: 'add' | 'event' | 'search'  | 'trash' | 'none' = 'none';

  /**
   * Variant defines type, stylingMode & may force icon usage
   */
  @Input() variant: 'primary' | 'secondary' | 'delete' = 'primary';

  /**
   * Button state
   */
  @Input() disabled?: boolean = false;

  /**
   * Emit click events
   */
  @Output() click = new EventEmitter();

  protected _onClick() { this.click.emit(); }

  protected readonly _variants: Record<
    'stylingMode' | 'type' | 'defaultIcon',
    Partial<Record<ButtonComponent['variant'], string>>
  > = {
    stylingMode: {
      secondary : "outlined",
      delete : "text",
    },
    type: {
      delete : "danger",
    },
    defaultIcon: {
      delete : "trash",
    }
  };
}
