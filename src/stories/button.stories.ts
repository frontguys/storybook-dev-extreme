import { Meta, StoryObj } from '@storybook/angular';
import { userEvent, expect } from '@storybook/test';

import { figmaFrame, check, onClickUpdate } from './utils';
import { ButtonComponent } from './button.component';

type Story = StoryObj<ButtonComponent>;
const meta: Meta<ButtonComponent> = {
  title: 'Components/Button',
  component: ButtonComponent,
  parameters: {
    actions: {
      handles: ['click'],
    },
  },
};
export default meta;


export const Primary: Story = {
  args: {
    label: "Primary action"
  },
  ...check((expected) => expected.toHaveStyle('background-color: rgb(240, 91, 65)')),
  ...figmaFrame('42-4')
};

export const Secondary: Story = {
  args: {
    label: "Secondary action",
    variant: "secondary",
  },
  ...check((expected) => expected.toHaveStyle('background-color: rgba(0, 0, 0, 0)')),
  ...figmaFrame('42-3')
};

export const Create: Story = {
  args: {
    label: "Add new item",
    icon: "add",
    disabled: false
  },
  decorators: [onClickUpdate({ icon: 'more', disabled: true }, 1000)],
  ...check(async (expected, button) => {
    await expected.toHaveTextContent('Add new item')
    await userEvent.click(button)
    await expected.toHaveAttribute('aria-disabled', 'true')
   }),
  ...figmaFrame('42-5')
};

export const Delete: Story = {
  args: {
    label: "Delete",
    variant: "delete",
    icon: "trash"
  },
  ...check((_, button) => expect(button.querySelector('i')).toHaveClass('dx-icon-trash')),
  ...figmaFrame('42-6')
};
