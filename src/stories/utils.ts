import { within, expect } from '@storybook/test';
import { useArgs } from '@storybook/preview-api'
import { Args, Decorator } from '@storybook/angular';

export const figma = (fileId: string) => (nodeId: string) => ({parameters: {
  design: {
    type: 'figma',
    url: `https://www.figma.com/file/${fileId}/?type=design&mode=design&node-id=${nodeId}`
  }
}})

export const figmaFrame = figma('RhniqVDJJEAbrfHjvygwRn');

export const check = (fn: (expected: ReturnType<typeof expect<HTMLElement>>, button: HTMLElement) => void  ) => ({
  play: ({ canvasElement }: { canvasElement: HTMLElement }) => {
    const button = within(canvasElement).getByRole('button')
    return fn(expect(button), button);
  }
})

export const onClickUpdate = (updatedArgs: Partial<Args>, timeout?: number): Decorator =>(Story, context) => {
  const [_, setArgs] = useArgs()
  return Story({ ...context, args: { ...context.args, click: () => {
    setArgs({...context.args, ...updatedArgs })
    if (timeout) setTimeout(() => setArgs(context.initialArgs), timeout)
 }}})
}
