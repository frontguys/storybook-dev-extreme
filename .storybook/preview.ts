import type { Preview } from "@storybook/angular";
import { setCompodocJson } from "@storybook/addon-docs/angular";
import { withActions } from '@storybook/addon-actions/decorator';
import docJson from "../documentation.json";
import themeLight from "./theme-light";

setCompodocJson(docJson);

const preview: Preview = {
  parameters: {
    backgrounds: { default: 'light' },
    controls: {
      exclude:/^_/g,
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    docs: {
      theme: themeLight,
    },
  },
  decorators: [withActions],
  argTypes: {
    click: { control: false },
    _onClick: { control: false },
  }
};

export default preview;
