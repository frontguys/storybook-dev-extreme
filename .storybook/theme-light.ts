import { create } from '@storybook/theming/create';

export default create({
  base: 'light',
  brandTitle: 'ODDO BHF',
  brandUrl: 'https://oddo-bhf.com/fr',
  brandImage: 'https://i.imgur.com/hOLBO1E.png',
  brandTarget: '_self',

  colorPrimary: '#d5281a',
  colorSecondary: '#013d39',
});